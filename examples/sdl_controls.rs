extern crate sdl2;
extern crate simple_synth;

use sdl2::keyboard::Keycode;
use simple_synth::synthesizer;

fn main() {
    let sdl_context = sdl2::init().unwrap();
    let video_subsys = sdl_context.video().unwrap();
    let window = video_subsys.window("Synthesizer", 640, 480).position_centered().opengl().build().unwrap();
    let mut canvas = window.into_canvas().build().unwrap();

    let mut synthesizer = synthesizer::Synthesizer::start(sdl_context.audio().unwrap());

    let mut channels: [Option<usize>; 12] = [Some(0usize); 12];

    let mut events = sdl_context.event_pump().unwrap();
    'main: loop {
        for event in events.poll_iter() {
            match event {
                sdl2::event::Event::Quit {..} => break 'main,
                sdl2::event::Event::KeyDown { keycode, repeat, .. } => if !repeat {
                    match keycode.unwrap() {
                        Keycode::A => { channels[0] = synthesizer.lock().play_note(261.6256); }, // C
                        Keycode::W => { channels[1] = synthesizer.lock().play_note(277.1826); }, // C#
                        Keycode::S => { channels[2] = synthesizer.lock().play_note(293.6648); }, // D
                        Keycode::E => { channels[3] = synthesizer.lock().play_note(311.1270); }, // D#
                        Keycode::D => { channels[4] = synthesizer.lock().play_note(329.6276); }, // E
                        Keycode::F => { channels[5] = synthesizer.lock().play_note(349.2282); }, // F
                        Keycode::T => { channels[6] = synthesizer.lock().play_note(369.9944); }, // F#
                        Keycode::G => { channels[7] = synthesizer.lock().play_note(391.9954); }, // G
                        Keycode::Y => { channels[8] = synthesizer.lock().play_note(415.3047); }, // G#
                        Keycode::H => { channels[9] = synthesizer.lock().play_note(440.0000); }, // A
                        Keycode::U => { channels[10] = synthesizer.lock().play_note(466.1638); }, // A#
                        Keycode::J => { channels[11] = synthesizer.lock().play_note(493.8833); }, // B
                        _ => ()
                    }
                },
                sdl2::event::Event::KeyUp { keycode, .. } => {
                    match keycode.unwrap() {
                        Keycode::A => {
                            if channels[0] != None {
                                synthesizer.lock().stop_note(channels[0].unwrap()).unwrap();
                            }
                        },
                        Keycode::W => {
                            if channels[1] != None {
                                synthesizer.lock().stop_note(channels[1].unwrap()).unwrap();
                            }
                        },
                        Keycode::S => {
                            if channels[2] != None {
                                synthesizer.lock().stop_note(channels[2].unwrap()).unwrap();
                            }
                        },
                        Keycode::E => {
                            if channels[3] != None {
                                synthesizer.lock().stop_note(channels[3].unwrap()).unwrap();
                            }
                        },
                        Keycode::D => {
                            if channels[4] != None {
                                synthesizer.lock().stop_note(channels[4].unwrap()).unwrap();
                            }
                        },
                        Keycode::F => {
                            if channels[5] != None {
                                synthesizer.lock().stop_note(channels[5].unwrap()).unwrap();
                            }
                        },
                        Keycode::T => {
                            if channels[6] != None {
                                synthesizer.lock().stop_note(channels[6].unwrap()).unwrap();
                            }
                        },
                        Keycode::G => {
                            if channels[7] != None {
                                synthesizer.lock().stop_note(channels[7].unwrap()).unwrap();
                            }
                        },
                        Keycode::Y => {
                            if channels[8] != None {
                                synthesizer.lock().stop_note(channels[8].unwrap()).unwrap();
                            }
                        },
                        Keycode::H => {
                            if channels[9] != None {
                                synthesizer.lock().stop_note(channels[9].unwrap()).unwrap();
                            }
                        },
                        Keycode::U => {
                            if channels[10] != None {
                                synthesizer.lock().stop_note(channels[10].unwrap()).unwrap();
                            }
                        },
                        Keycode::J => {
                            if channels[11] != None {
                                synthesizer.lock().stop_note(channels[11].unwrap()).unwrap();
                            }
                        },
                        _ => ()
                    }
                }
                _ => ()
            }
        }

        canvas.set_draw_color(sdl2::pixels::Color::RGB(127, 127, 127));
        canvas.clear();
        canvas.present();
    }
}
