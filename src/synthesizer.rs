//! This modules contains the Synthesizer struct that you will need to play
//! notes.

use std::mem;

use sdl2::audio::AudioCallback;
use sdl2::AudioSubsystem;
use sdl2::audio::AudioSpecDesired;
use sdl2::audio::AudioDevice;

use ::components::Component;
use ::components::oscillator::*;
use ::components::amplifier::Amplifier;
use ::components::input::Input;
use ::components::filter::Filter;
use ::components::adsr::*;

const MAX_SIMULTANEOUS_SOUNDS: usize = 5;

/// Main structure representing the virtual synthesizer.
pub struct Synthesizer {
    oscillators: [Oscillator; MAX_SIMULTANEOUS_SOUNDS],
    amplifiers: [Amplifier; MAX_SIMULTANEOUS_SOUNDS],
    inputs: [Input; MAX_SIMULTANEOUS_SOUNDS],
    filters: [Filter; MAX_SIMULTANEOUS_SOUNDS],
    adsrs: [ADSR; MAX_SIMULTANEOUS_SOUNDS],
}

impl AudioCallback for Synthesizer {
    type Channel = f32;

    /// The callback is called by SDL2 at a fixed frequency (44100 Hz by default) to generate an amplitude in [-1; 1].
    fn callback(&mut self, out: &mut [f32]) {
        for x in out.iter_mut() {
            let mut output = 0.0f32;
            for i in 0..MAX_SIMULTANEOUS_SOUNDS {
                output += self.amplifiers[i].generate(
                    self.filters[i].generate(
                        self.adsrs[i].generate(
                            self.oscillators[i].generate(
                                self.inputs[i].velocity
                            ),
                        ),
                    ),
                );

                self.oscillators[i].tick();
                self.amplifiers[i].tick();
                self.filters[i].tick();
                self.adsrs[i].tick();
            }

            *x = output;
        }
    }
}

impl Synthesizer {
    /// Create a new SDL2 `AudioDevice` around the `Synthesizer` that will handle the dialogue between the synth and the audio driver.
    pub fn start(subsystem: AudioSubsystem) -> AudioDevice<Synthesizer> {
        let spec = AudioSpecDesired {
            freq: Some(44100),
            channels: Some(1),
            samples: None
        };

        let device = subsystem.open_playback(None, &spec, |spec| {
            let (oscillators, amplifiers, inputs, filters, adsrs) = unsafe {
                let mut oscillators: [Oscillator; MAX_SIMULTANEOUS_SOUNDS] = mem::uninitialized();
                let mut amplifiers: [Amplifier; MAX_SIMULTANEOUS_SOUNDS] = mem::uninitialized();
                let mut inputs: [Input; MAX_SIMULTANEOUS_SOUNDS] = mem::uninitialized();
                let mut filters: [Filter; MAX_SIMULTANEOUS_SOUNDS] = mem::uninitialized();
                let mut adsrs: [ADSR; MAX_SIMULTANEOUS_SOUNDS] = mem::uninitialized();
                for i in 0..MAX_SIMULTANEOUS_SOUNDS {
                    oscillators[i] = Oscillator::new(spec.freq, Waveform::Sawtooth);
                    amplifiers[i] = Amplifier::new(0.5, spec.freq, None);
                    inputs[i] = Input::new();
                    filters[i] = Filter::new(spec.freq, 800.0, Some((20.0, 0.1)));
                    adsrs[i] = ADSR::new((spec.freq as f32 * 0.15) as i32, (spec.freq as f32 * 0.1) as i32, (spec.freq as f32 * 0.1) as i32);
                }

                (oscillators, amplifiers, inputs, filters, adsrs)
            };

            Synthesizer {
                oscillators,
                amplifiers,
                inputs,
                filters,
                adsrs,
            }
        }).unwrap();

        device.resume();

        device
    }

    /// Finds a free channel and starts playing a note on said channel, then returns the channel. If a channel can't be found (all channels are busy), returns None.
    /// Please hold on to the channel, as it will be needed to stop playing the note.
    pub fn play_note(&mut self, frequency: f32) -> Option<usize> {
        let mut channel: Option<usize> = None;
        for i in 0..MAX_SIMULTANEOUS_SOUNDS {
            if self.inputs[i].gate == false && !self.adsrs[i].is_on() {
                channel = Some(i);
                break;
            }
        }

        if channel == None {
            return channel;
        }

        self.inputs[channel.unwrap()].gate = true;
        self.inputs[channel.unwrap()].velocity = 1.0;
        self.oscillators[channel.unwrap()].frequency = frequency;

        self.adsrs[channel.unwrap()].attack();

        channel
    }

    /// Stops playing the note on the given channel. Returns an error if the channel doesn't exist.
    pub fn stop_note(&mut self, channel: usize) -> Result<(), &str> {
        if(channel >= MAX_SIMULTANEOUS_SOUNDS) {
            return Err("Invalid channel (there aren't that many channels available)");
        }

        self.inputs[channel].gate = false;

        self.adsrs[channel].release();

        Ok(())
    }
}
