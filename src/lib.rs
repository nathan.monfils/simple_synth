//! This is documentation for `simple_synth`.
//! 
//! Simple synth is a crate I created for fun that can play simple synthesized
//! notes over SDL2.
//! 
//! The `synthesizer::Synthesizer` struct is the one you want to start playing notes.
//! 
//! # Examples
//! 
//! ```
//! // ...
//! // Usual SDL2 setup
//! // ...
//! 
//! let mut synthesizer = Synthesizer::start(sdl_context.audio().unwrap());
//! let channel = synthesizer.lock().play_note(400.0);
//! std::thread::sleep(std::time::Duration::from_millis(500));
//! synthesizer.lock().stop_note(channel.unwrap());
//! ```

extern crate sdl2;

pub mod synthesizer;
pub mod components;
