//! An `Amplifier` takes an input amplitude and multiplies it by a static
//! `volume`.

use ::components::Component;
use ::components::lfo::*;

pub struct Amplifier {
    volume: f32,
    lfo: Option<(LFO, f32)>
}

impl Amplifier {
    /// Create a new `Amplifier` with a volume of `volume` for a SDL polling
    /// rate of `output_frequency`, with an optional `lfo_frequency_amplitude`,
    /// which is a struct containing the LFO's frequency then amplitude.
    pub fn new(volume: f32, output_frequency: i32, lfo_frequency_amplitude: Option<(f32, f32)>) -> Amplifier {
        Amplifier {
            volume,
            lfo: match lfo_frequency_amplitude {
                Some(f) => Some((LFO::new(output_frequency, f.0), f.1)),
                None => None,
            },
        }
    }
}

impl Component for Amplifier {
    fn tick(&mut self) {
        match &mut self.lfo {
            Some(lfo) => lfo.0.tick(),
            None => ()
        }
    }

    fn generate(&mut self, input: f32) -> f32 {
        let volume = match &mut self.lfo {
            Some(lfo) => self.volume + self.volume * lfo.0.generate(lfo.1),
            None => self.volume
        };
        input * volume
    }
}
