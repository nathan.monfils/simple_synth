//! An `ADSR` is en enveloppe generator following the "Attack, Decay, Sustain,
//! Release" principle. This one has configurable Attack, Decay and Release
//! times.

use ::components::Component;

#[derive(Copy, Clone)]
pub enum ADSRState {
    Attack,
    Decay,
    Sustain,
    Release
}

pub struct ADSR {
    on: bool,
    gate: bool,

    state: ADSRState,
    tick: i32,
    attack_ticks: i32,
    decay_ticks: i32,
    release_ticks: i32,

    release_start_amplitude: f32,
}

impl ADSR {
    /// Creates a new `ADSR` with corresponding Attack, Decay and Release
    /// times in ticks (a tick is one SDL poll, by default at 44100 Hz).
    pub fn new(attack_ticks: i32, decay_ticks: i32, release_ticks: i32) -> ADSR {
        ADSR {
            on: true,
            gate: false,

            state: ADSRState::Attack,
            tick: 0,
            attack_ticks: attack_ticks,
            decay_ticks: decay_ticks,
            release_ticks: release_ticks,

            release_start_amplitude: 0.0,
        }
    }

    /// Start playing a new note (puts the ADSR in Attack state).
    pub fn attack(&mut self) {
        self.on = true;
        self.gate = true;

        self.state = ADSRState::Attack;
        self.tick = 0;
    }

    /// Stop playing the current note (puts the ADSR in Release state, meaning
    /// sound will still be produced for the duration of the Release state).
    pub fn release(&mut self) {
        self.gate = false;
    }

    /// Returns `true` if the ADSR is on (playing a note), `false` otherwise.
    /// Used to determine if the channel is used or not.
    pub fn is_on(&self) -> bool {
        self.on
    }
}

impl Component for ADSR {
    fn generate(&mut self, input: f32) -> f32 {
        let line = |a: (f32, f32), b: (f32, f32), x: f32| -> f32 {
            let m = (a.1 - b.1) / (a.0 - b.0);
            let p = a.1 - m * a.0;

            m * x + p
        };

        input * match self.state {
            ADSRState::Attack => line((0.0, 0.0), (self.attack_ticks as f32, 1.0), self.tick as f32),
            ADSRState::Decay => line((0.0, 1.0), (self.decay_ticks as f32, 0.7), self.tick as f32),
            ADSRState::Sustain => 0.7,
            ADSRState::Release => {
                if self.on {
                    line((0.0, self.release_start_amplitude), (self.release_ticks as f32, 0.0), self.tick as f32)
                } else {
                    0.0
                }
            }
        }
    }

    fn tick(&mut self) {
        self.tick += 1;

        let current_state = self.state.clone();
        match current_state {
            ADSRState::Attack => {
                if !self.gate {
                    self.release_start_amplitude = self.generate(1.0);
                    self.tick = 0;
                    self.state = ADSRState::Release;
                } else if self.tick > self.attack_ticks {
                    self.tick = 0;
                    self.state = ADSRState::Decay;
                }
            },
            ADSRState::Decay => {
                if !self.gate {
                    self.release_start_amplitude = self.generate(1.0);
                    self.tick = 0;
                    self.state = ADSRState::Release;
                } else if self.tick > self.decay_ticks {
                    self.tick = 0;
                    self.state = ADSRState::Sustain;
                }
            },
            ADSRState::Sustain => {
                if !self.gate {
                    self.release_start_amplitude = self.generate(1.0);
                    self.tick = 0;
                    self.state = ADSRState::Release;
                }
            },
            ADSRState::Release => {
                if self.tick > self.release_ticks {
                    self.tick = 0;
                    self.on = false;
                }
            }
        }
    }
}