//! An `Input` contains information about the last key press.

pub struct Input {
    pub gate: bool,
    pub velocity: f32,
}

impl Input {
    /// Create a new `Input` with no key press.
    pub fn new() -> Input {
        Input {
            gate: false,
            velocity: 0.0,
        }
    }
}
