//! This module contains the different components that make a synthesizer.
//! Check [Beau Sievers' blog](http://beausievers.com/synth/synthbasics/) for an overview of how they work together.

/// All dynamic Components share this trait. `tick()` must be called once a tick, `generate()` creates an output amplitude based on the input amplitude.
/// 
/// `generate()` requires a mutable self-reference because the `Filter` component, by definition, requires updating for each amplitude generated.
pub trait Component {
    fn generate(&mut self, input: f32) -> f32;
    fn tick(&mut self);
}

pub mod input;
pub mod oscillator;
pub mod lfo;
pub mod filter;
pub mod adsr;
pub mod amplifier;
