//! A `Filter` is a first-order low-pass filter used to make a much more
//! pleasing sound.

use ::components::Component;
use ::components::lfo::*;

use std::f64::consts::PI;

pub struct Filter {
    output_frequency: i32,
    cutoff: f32,

    lfo: Option<(LFO, f32)>, // LFO, amplitude

    x: f32
}

impl Filter {
    /// Create a new `Filter` for a SDL polling rate of `output_frequency`,
    /// with a cutoff frequency of `cutoff` and with an optional
    /// `lfo_frequency_amplitude`, which is a struct containing the LFO's
    /// frequency then amplitude.
    pub fn new(output_frequency: i32, cutoff: f32, lfo_frequency_amplitude: Option<(f32, f32)>) -> Filter {
        Filter {
            output_frequency,
            cutoff,

            lfo: match lfo_frequency_amplitude {
                Some(f) => Some((LFO::new(output_frequency, f.0), f.1)),
                None => None,
            },

            x: 0.0,
        }
    }
}

impl Component for Filter {
    fn tick(&mut self) {
        match &mut self.lfo {
            Some(lfo) => lfo.0.tick(),
            None => ()
        }
    }

    // This is a simple first-order low pass filter. y[i] := y[i - 1] + alpha * (x[i] - y[i-1]).
    // Good resources for neophytes (like yours truly):
    // https://en.wikipedia.org/wiki/Low-pass_filter#Discrete-time_realization (not directly applicable here as we have a stream instead of a fixed input)
    // https://kiritchatterjee.wordpress.com/2014/11/10/a-simple-digital-low-pass-filter-in-c/
    fn generate(&mut self, x: f32) -> f32 {
        let cutoff = match &mut self.lfo {
            Some(lfo) => self.cutoff + self.cutoff * lfo.0.generate(lfo.1),
            None => self.cutoff
        };

        let dt = 1.0 / self.output_frequency as f32;
        let rc = 1.0 / (2.0 * PI as f32 * cutoff);
        let alpha = dt / (rc + dt);
        self.x = self.x + alpha * (x - self.x);

        self.x
    }
}
