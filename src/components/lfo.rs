//! An `LFO` is a low frequency oscillator used to vary the parameters or output
//! of other Components.

use ::components::Component;

use std::f64::consts::PI;

pub struct LFO {
    output_frequency: i32, // Frequency of the SDL output (44100 Hz by default)
    frequency: f32, // Frequency of the oscillating signal

    ticks: i32,
}

impl LFO {
    /// Create a new `LFO` for a SDL polling rate of `output_frequency` and with
    /// a fixed frequency of `frequency` (typically 1-20 Hz).
    pub fn new(output_frequency: i32, frequency: f32) -> LFO {
        LFO {
            output_frequency,

            frequency,
            ticks: 0,
        }
    }
}

impl Component for LFO {
    fn tick(&mut self) {
        self.ticks = (self.ticks + 1) % ((self.output_frequency as f32) / (self.frequency as f32)) as i32;
    }

    fn generate(&mut self, input: f32) -> f32 {
        let x = (2.0 * PI as f32 * self.ticks as f32 / ((self.output_frequency as f32) / self.frequency)).sin();

        x * input
    }
}
